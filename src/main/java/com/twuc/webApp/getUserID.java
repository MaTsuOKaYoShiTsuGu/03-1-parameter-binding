package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@RestController
public class getUserID {
    @GetMapping("/api/users/{userID}")
    String getIDByInteger(@PathVariable Integer userID){
        return String.valueOf(userID);
    }

    @GetMapping("/api/users2/{userID}")
    String getIDByInt(@PathVariable int userID){
        return String.valueOf(userID);
    }

    @GetMapping("/api/users/{userId}/books/{bookId}")
    String getDoubleID(@PathVariable int userId,@PathVariable int bookId){
        return String.valueOf(userId)+" "+String.valueOf(bookId);
    }

    @GetMapping("/api/users3/")
    String getRequestParam(@RequestParam String name){
        return name;
    }

    @GetMapping("/api/users4/")
    String getRequestParamWithoutValue(@RequestParam String name){
        return name;
    }

    @GetMapping("/api/users5/")
    String getRequestParamWithDefaultValue(
            @RequestParam(
                    value="name",
                    required = false,
                    defaultValue = "kayanon") String name){
        return name;
    }

    @GetMapping("/api/users6/")
    List<String> getCollection(
            @RequestParam List<String> name){
        return name;
    }

    ObjectMapper om = new ObjectMapper();

    @GetMapping("/api/jackson/")
    String getSerializedData() throws JsonProcessingException {
        Person person = new Person("kayanon");
        return om.writeValueAsString(person);
    }

    @PostMapping("/api/jackson2/")
    Person getUnserializedData(@RequestBody String json) throws IOException {
        return om.readValue(json,Person.class);
    }

    @PostMapping("/api/datetimes/")
    LocalDate getDateTimeJson(@RequestBody String dateTime) throws IOException {
        return om.readValue(dateTime, LocalDate.class);
    }

    @PostMapping("/api/pass-param")
    String getPassParam(@RequestBody @Valid String json) throws IOException {
        return om.readValue(json,Person.class).getName();
    }

    @PostMapping("/api/pass-no-param")
    // String @RequestParam
    Person getPassWithoutParam(@RequestBody @Valid Person person) {
        return person;
    }

    @PostMapping("/api/pass-param-in-bound")
    Person getPassWithParamInBound(@RequestBody @Valid Person person) {
        return person;
    }

    @PostMapping("/api/email")
    Person getPersonWithValidEmail(@RequestBody @Valid Person person){
        return person;
    }
}
