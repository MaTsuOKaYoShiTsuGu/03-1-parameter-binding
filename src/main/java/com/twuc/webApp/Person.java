package com.twuc.webApp;

import javax.validation.constraints.*;

public class Person{
    @NotNull
    @Size(min = 0,max = 10)
    String name;

    @Min(1000)
    @Max(9999)
    int yearOfBirth;

    @Email
    String email;
    public Person(String name,int yearOfBirth){
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Person() {
    }

    public Person(@NotNull @Size(min = 0, max = 10) String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
