package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class getUserIDIntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Test
    void should_return_userID_when_get_userID_by_int() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/3"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("3"));
    }

    @Test
    void should_return_userID_when_get_userID_by_Integer() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users2/3"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("3"));
    }

    @Test
    void should_binding_double_para() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2/books/3"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("2 3"));
    }

    @Test
    void should_bind_para_successful() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users3/?name=kayanon"))
                .andExpect(MockMvcResultMatchers.content().string("kayanon"));
    }

    @Test
    void should_return_empty_value_without_para() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users4/"))
                .andExpect(MockMvcResultMatchers.content().string(""));
    }

    @Test
    void should_return_default_value() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users5/"))
                .andExpect(MockMvcResultMatchers.content().string("kayanon"));
    }

    @Test
    void should_return_collection() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users6/?name=kayanon&name=ayaneru"))
                .andExpect(MockMvcResultMatchers.content().string("[\"kayanon\",\"ayaneru\"]"));
    }
    private ObjectMapper om = new ObjectMapper();

    @Test
    void should_serialize_object_to_json() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/jackson"))
                .andExpect(MockMvcResultMatchers.content().string("{\"name\":\"kayanon\"}"));
    }

    @Test
    void should_unserialize_json_to_object() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/jackson2")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"kayanon\"}"))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(jsonPath("$.name").value("kayanon"));
    }

    @Test
    void should_failed_to_get_date_time_from_json() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"dateTime\":\"2019-10-01T10:00:00Z\"}"))
                .andExpect(status().is(404));
                //.andExpect(jsonPath("$.dateTime").value("2019-10-01T10:00:00Z"));
    }

    @Test
    void should_pass_with_param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/pass-param")
                .content(om.writeValueAsString(new Person("kayanon"))))
                .andExpect(MockMvcResultMatchers.content().string("kayanon"));
    }

    @Test
    void should_failed_when_without_param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/pass-no-param")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(om.writeValueAsString(new Person(null))))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_failed_when_name_size_out_of_bound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/pass-no-param")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(om.writeValueAsString(new Person("KasumigaokaUtaha"))))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_pass_when_year_of_birth_in_bound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/pass-param-in-bound")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(om.writeValueAsString(new Person("kayanon",1987))))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_failed_when_year_of_birth_out_of_bound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/pass-param-in-bound")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(om.writeValueAsString(new Person("ayaneru",999))))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_pass_when_email_is_valid() throws Exception {
        Person person = new Person("kayanon",1987);
        person.setEmail("kayanon@gmail.com");
        mockMvc.perform(MockMvcRequestBuilders.post("/api/email")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(om.writeValueAsString(person)))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_failed_when_email_is_invalid() throws Exception {
        Person person = new Person("kayanon",1987);
        person.setEmail("???");
        mockMvc.perform(MockMvcRequestBuilders.post("/api/email")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(om.writeValueAsString(person)))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

}